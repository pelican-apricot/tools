# for running Pelican and web server
# Written by Yu-Jie Lin in 2016

PORT ?= 8000

all:
	pelican content
.PHONY: all

server:
	cd output; python -m pelican.server
.PHONY: server

dev:
	./devserver.sh ${PORT}
.PHONY: dev
