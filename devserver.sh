#!/bin/bash
# Development server helper script
#
# Written by Yu-Jie Lin in 2016
#
# This script has been placed in the public domain, or via UNLICENSE,
# if not applicable.
#
# You can press [q] to quit and [r] to restart Pelican.


CONTENT_DIR='content'
OUTPUT_DIR='output'
SETTINGS_FILE='pelicanconf.py'


run_server()
{
  cd output
  python -m pelican.server "$1" &
  PELICAN_SERVER_PID=$!
  cd ..
}


run_pelican()
{
  pelican                         \
    --verbose                     \
    --debug                       \
    --autoreload                  \
    --relative-urls               \
    --settings "$SETTINGS_FILE"   \
    --output "$OUTPUT_DIR"        \
    "$CONTENT_DIR"                \
    &
  PELICAN_PID=$!
}


cleanup()
{
  echo 'exiting...'
  kill $PELICAN_SERVER_PID
  kill $PELICAN_PID
  exit 0
}


main()
{
  trap 'cleanup' INT TERM EXIT;

  run_pelican
  run_server "$1"
  while :; do
    read -t 0.01 -n 1
    case "$REPLY" in
      [rR])
        kill $PELICAN_PID
        run_pelican
        ;;
      [qQ])
        break
        ;;
    esac
  done
}


main "$1"
