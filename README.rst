=======================
Tools for using Pelican
=======================

I wrote these tools for using Pelican, the ones from ``pelican-quickstart``
just didn't work for my liking, so I wrote my own and kept some of their names
and a few behaviors, but I didn't steal any codes, I needed them to be in the
public domain.


.. contents:: **Contents**
   :local:


Makefile targets
================

``all``
-------

Runs ``pelican content``.

``server``
----------

Runs Pelican web server.

``dev``
-------

See |devserver.sh|_. In Pelican's Makefile, it's named ``devserver``, that is
way too more characters, simple ``dev`` should suffice. You can use
``PORT=1234`` to specify the port number.

.. |devserver.sh| replace:: ``devserver.sh``


``devserver.sh``
================

It runs Pelican and web server, currently only supports one argument for port
number..

The Pelican is started with the following options::

  --verbose --debug --autoreload --relative-urls

It accepts key inputs:

``[r]``
  Restarts Pelican.
  
  With ``--verbose`` and ``--debug``, it won't reload when template has errors
  and it doesn't quit either, just hanging there. And this is the drawback of
  the official script, I added this feature for restarting Pelican.
  
``[q]``
  Gracefully quits.


Contributing
============

You are welcome for any pull requests, but by opening a request, you agree to
place your contribution in the public domain and license under the UNLICENSE_
for where not applicable.

If you don't agree, do not open a pull request.


Copyright
=========

The contents in this repository have been placed in the public domain, or via
UNLICENSE_, if not applicable.

.. _UNLICENSE: UNLICENSE
